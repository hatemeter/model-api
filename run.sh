#!/bin/bash


PORT_BIND=8080

if [[ $# -gt 0 ]]; then
    PORT_BIND=$1
    shift
fi

cd api/
gunicorn --bind 0.0.0.0:${PORT_BIND} wsgi
