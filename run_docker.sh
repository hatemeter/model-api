#!/bin/bash


if [[ -z ${IMAGE_NAME} ]]; then
    IMAGE_NAME="hatemeter"
fi

BIND_PORT=8080
if [[ $# -ne 0 ]]; then
    BIND_PORT=$1
    shift
fi


docker build -t ${IMAGE_NAME} .

docker container run -it --rm -p ${BIND_PORT}:8080 ${IMAGE_NAME}
