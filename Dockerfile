FROM python:3.8

WORKDIR /opt/app

RUN pip install --upgrade pip && pip install --upgrade pipenv

COPY api/ /opt/app/api
COPY Pipfile /opt/app/
COPY Pipfile.lock /opt/app/

RUN pipenv install --system --deploy --ignore-pipfile

WORKDIR /opt/app/api

ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:8080", "wsgi"]
