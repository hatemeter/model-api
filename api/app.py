from flask import Flask, request, jsonify
from hatemeter_model import model


app = Flask(__name__)


@app.route('/')
def url_doc():
    # TODO: Use Swagger lib like connection
    urls = {
        'predict': {
            'endpoint': '/predict/tweet',
            'method': 'GET',
            'params_type': 'x-www-form-urlencoded',
            'params_list': [
                'content'
            ]
        }
    }
    response = jsonify(urls)
    response.status_code = 200
    return response


@app.route('/predict/tweet')
def hello_world():
    content = request.args.get('content')
    if not content:
        err = {'error': 'Need the content parameter'}
        response = jsonify(err)
        response.status_code = 400
        return response
    love = model.love(content)
    result = {'love': love}
    response = jsonify(result)
    response.status_code = 200
    return response


if __name__ == '__main__':
    # WARNING: Only for development!
    app.run(port=8080)
