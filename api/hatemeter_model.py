from hatemeter.logistic_regression import LogisticRegression


# Load a pre-trained model
model = LogisticRegression()
model.load_module_model()
